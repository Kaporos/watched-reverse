import requests
from tokens import TokenCatcher
from fuzzywuzzy import fuzz
from operator import itemgetter, attrgetter
import joblib
import multiprocess
class Addon:
    def __init__(self,host,id,base=None):
        if base == None:
            self.json = requests.get(host+"/"+id+"/addon.watched").json()
        else:
            self.json = base
        self.id = self.json["id"]
        print("Detected "+id)

        self.type = self.json["type"]
        self.host = host

        if self.type == "worker":
            self.actions = self.json["actions"]
        if self.type == "bundle":
            print("BUUUUNDLE")
            self.requirements = self.json["requirements"]
    def __str__(self):
        return self.id
class Server:
    def proceedBundle(self,bundle):
        requirements = bundle.requirements
        added_workers = []
        for requirement in requirements:
            if type(requirement) == dict:
                url = requirement["url"]
                id = requirement["id"]
                if(url[-1] == "/"):
                    url = url[:-1]
                if not url in self.engine.bundles_parts:
                    print(url+"is not in ")
                    print(self.engine.bundles_parts)
                    print("Proceeding bundle part : "+url)
                    self.engine.scrawl(url)
            else:
                pass
    def __init__(self,host,base,type,engine):
        if type == "server":
            addons = base["addons"]
        else:
            url = (host+"/repository.watched")
            addons = requests.get(url).json()
        self.host = host
        self.workers = []
        self.engine = engine

        for addon in addons:
            if type == "repository":
                addon = Addon(host,addon["id"],addon)
            else:
                addon = Addon(host,addon)
            if addon.type == "worker":
                self.workers.append(addon)
            if addon.type == "bundle":
                self.engine.bundles_parts.append(host)
                self.proceedBundle(addon)




class WatchedEngine:
    def __init__(self):
        self.servers = []
        self.repositories = []
        self.workers = []
        self.workers_ids = []
        self.bundles_parts = []
        self.catcher = TokenCatcher(5687)
    def scrawl(self,host):
        base = requests.get(host+"/addon.watched").json()
        base_type = base["type"]
        server = Server(host,base,base_type,self)
        for addon in server.workers:
            if addon.id in self.workers_ids:
                print("ADDON already registered")
            else:
                print("NEW ADDON : "+addon.id)
                self.workers.append(addon)
                self.workers_ids.append(addon.id)
    def get_workers(self,actions=[],itemTypes=[]):
        result = []
        for worker in self.workers:
            checked = True
            for action in actions:
                if action in worker.actions:
                    pass
                else:
                    checked = False
            if("itemTypes" in worker.json.keys()):
                for itemtype in itemTypes:
                    if itemtype in worker.json["itemTypes"]:
                        pass
                    else:
                        checked = False
            else:
                if(len(itemTypes) > 0):
                    checked = False
            if checked:
                result.append(worker)
        return result
    def search(self,search):
        result = []
        directory_workers = self.get_workers(["directory"])
        token = self.catcher.catch()
        for worker in directory_workers:
            if "rootDirectories" in worker.json:
                print(worker.__str__()+" there is some root")
                for root in worker.json["rootDirectories"]:
                    print(root["id"])
                    data = {"language":"fr","region":"BE","rootId":root["id"],"id":"","adult":False,"search":search,"sort":"","filter":{},"cursor":1,"page":1}
                    response = requests.post(worker.host+"/"+worker.id+"/"+"directory.watched", json = data, headers={"watched-sig":token}).json()
                    if "items" in response.keys():
                        print(worker.__str__()+"("+root["id"]+")------->"+str(len(response["items"]))+" résultats")
                        for item in response["items"]:
                            print("-------------->"+item["name"])
                            result.append((item,fuzz.ratio(item["name"],search)))

            else:
                data = {"language":"fr","region":"BE","rootId":"","id":"","adult":False,"search":search,"sort":"","filter":{},"cursor":1,"page":1}

                response = requests.post(worker.host+"/"+worker.id+"/"+"directory.watched", data = data, headers={"watched-sig":token}).json()

                if "items" in response.keys():
                    print(worker.__str__()+"------->"+str(len(response["items"]))+" résultats")
                    for item in response["items"]:
                        print("-------------->"+item["name"])
                        result.append((item,fuzz.ratio(item["name"],search)))

            #print('Searching for '+search+' in '+worker.__str__())
        results = sorted(result,key=itemgetter(1))
        results.reverse()
        for result in results:
            print("------------------------------------")
            print(result[0]["name"])
            if "ids" in result[0].keys():
                print(result[0]["ids"])

            if "sources" in result[0].keys():
                print(result[0]["sources"])

            print("------------------------------------")

        return result
    def getUrls(self,name="",id="",episode=0,season=0,elem_type="movie",language="",imdb=False,subtitles=False,forceImdb=False):

        if not subtitles:
            workers = self.get_workers(actions=["source"],itemTypes=[elem_type])
        else:
            workers = self.get_workers(actions=["subtitle"],itemTypes=[elem_type])
        sources = []
        token = self.catcher.catch()
        if(imdb or subtitles):
            if elem_type == "movie":
                idToImdb = requests.get("https://api.themoviedb.org/3/movie/"+str(id)+"?api_key=3278d317b477f41e4ff3ac8be9c25c49").json()["imdb_id"]
            elif elem_type == "series":
                idToImdb = requests.get("https://api.themoviedb.org/3/tv/"+str(id)+"?api_key=3278d317b477f41e4ff3ac8be9c25c49&append_to_response=external_ids").json()["external_ids"]["imdb_id"]


        for worker in workers:
            try:
                print(worker.__str__()+" is working....")

                #print(worker.__str__())
                responses_tot = []
                if not forceImdb:
                    data = {"type":elem_type,"ids":{"tmdb_id":id},"name":name}
                else:
                    data = {"type":elem_type,"ids":{"imdb_id":id},"name":name}
                if(elem_type=="series"):
                    data["episode"] = {"episode":episode,"season":season}
                if not subtitles:
                    print("Proceeding classic ......")
                    response = requests.post(worker.host+"/"+worker.id+"/"+"source.watched", json = data, headers={"watched-sig":token}).json()
                    responses_tot.append(response)
                    if(imdb):
                        data = {"type":elem_type,"ids":{"imdb_id":idToImdb},"name":name}
                        if(elem_type=="series"):
                            data["episode"] = {"episode":episode,"season":season}
                        print("Proceeding IMDB ......")
                        response = requests.post(worker.host+"/"+worker.id+"/"+"source.watched", json = data, headers={"watched-sig":token}).json()
                        responses_tot.append(response)
                else:
                    data = {"type":elem_type,"ids":{"imdb_id":idToImdb},"name":name}
                    if(elem_type=="series"):
                        data["episode"] = {"episode":episode,"season":season}
                    response = requests.post(worker.host+"/"+worker.id+"/"+"subtitle.watched", json = data, headers={"watched-sig":token}).json()
                    responses_tot.append(response)
                for response in responses_tot:

                    if  type(response) == list:
                        print(worker.__str__()+" ---->"+str(len(response))+" résultats")
                        if(len(response) > 0):
                            for elem in response:
                                try:

                                    if(len(language) > 1):
                                        if not subtitles:
                                            if(language in elem["languages"]):
                                                sources.append({"url":elem["url"],"languages":elem["languages"]})
                                        else:
                                            if(language == elem["language"]):
                                                sources.append({"url":elem["url"],"language":elem["language"]})
                                    else:
                                        if(subtitles):
                                            sources.append({"url":elem["url"],"language":elem["language"]})
                                        else:
                                            sources.append({"url":elem["url"],"languages":elem["languages"]})
                                except:
                                    pass
            except:
                pass

        return  [i for n, i in enumerate(sources) if i not in sources[n + 1:]]
