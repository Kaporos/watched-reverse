from flask import Flask, jsonify,request
import logging
import sys, time
import asyncio

#cli = sys.modules['flask.cli']
#cli.show_server_banner = lambda *x: None

token = ""


def shutdown_server():
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        raise RuntimeError('Not running with the Werkzeug Server')
    func()





class TokenCatcher:
    def __init__(self,port):
        self.token = ""
        self.host = "0.0.0.0"
        self.port = port
    async def checkToken(self):
        time.sleep(2)
        if(self.token != ""):
            func = request.environ.get('werkzeug.server.shutdown')
            if func is None:
                raise RuntimeError('Not running with the Werkzeug Server')
            func()
    def be_fake(self):
        return jsonify({
          "type": "server",
          "addons": [
            "fake"
          ]
        })
    def index(self):
        return "La purée c cool"
    def get_token(self):
        self.token = request.headers.get('watched-sig')
        asyncio.run(self.checkToken())
        return jsonify({"actions":[],"id":"External Auth","name":"0Auth External","version":"0.0.0","itemTypes":[],"requestArgs":[],"type":"worker","urlPatterns":[],"sdkVersion":"0.31.1"})


    def catch(self):
        self.token = ""
        app = Flask(__name__)

        app.add_url_rule('/addon.watched',"be_fake",self.be_fake,methods=["GET"])
        app.add_url_rule('/fake/addon.watched',"get_token",self.get_token,methods=["POST"])
        app.add_url_rule('/',"index",self.index,methods=["GET"])

        app.run(host=self.host,port=self.port)

        return self.token
